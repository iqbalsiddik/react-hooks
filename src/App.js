import "./App.css";
import { useSelector } from "react-redux";

export default function App() {
  const stateReducer = useSelector((state) => state.homeReducer);

  return (
    <div className="flex flex-row bg-red-400">
      <div className="basis-1/4">01</div>
      <div className="basis-1/4">02</div>
      <div className="basis-1/2">03</div>
    </div>
  );
}
